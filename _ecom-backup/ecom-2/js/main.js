(function ($) {
"use strict";

    $(".zoom-01").elevateZoom({tint:true, tintColour:'#F90', tintOpacity:0.5});

    // menu-sidebar-button
    $('.menu-sidebar-button').on('click', function(event) {
        $('.fixed-sidebar').toggleClass('open');
        event.preventDefault();
    });


    // menu-sidebar-button
    $('.sidebar-right-bars').on('click', function(event) {
        $('.fixed-sidebar.right').toggleClass('expand');
        event.preventDefault();
    });


    // // menu-sidebar-button
    // $('.responsive-menu-btn').on('click', function(event) {
    //     $('.responsive-menu-list').addClass('show');
    //     event.preventDefault();
    // });

    $(".responsive-menu-btn , .responsive-menu-list").hover(function(){
        $('.responsive-menu-list').toggleClass('show');
        event.preventDefault();
    });
    
    // menu-sidebar-button
    $('.responsive-sidebar-btn, .responsive-sidebar-btn-close').on('click', function(event) {
        $('.responsive-sidebar').toggleClass('show');
        event.preventDefault();
    });

//     // mean-menu
//     $('#menu').meanmenu({
//        meanMenuContainer: '.mean-menu',
//        meanScreenWidth: "991",
//        onePage: false,
//    });


    // niceSelect
    $('select').niceSelect();

    // // sticky-header
    // $(window).on('scroll', function () {
    //     var scroll = $(window).scrollTop();
    //     if (scroll < 1) {
    //         $(".header-area").removeClass("sticky");
    //     } else {
    //         $(".header-area").addClass("sticky");
    //     }
    // });

    // WOW active
    new WOW().init();

    // magnificPopup img view
    $('.popup-image').magnificPopup({
        type: 'image',
        gallery: {
          enabled: true
        }
    });

    // magnificPopup video view
    $('.popup-video').magnificPopup({
        type: 'iframe'
    });
    
    // brand-logo-carousel
    $('.brand-logo-carousel').owlCarousel({
        loop: true,
        margin: 10,
        items: 4,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            767: {
                items: 2
            },
            992: {
                items: 4
            },
            1600: {
                items: 6
            }
        }
    });
    
    // products-carousel-card
    $('.products-carousel-card').owlCarousel({
        loop: true,
        margin: 15,
        items: 3,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            767: {
                items: 2
            },
            992: {
                items: 3
            }
        }
    });

    // products-carousel-card-4-item
    $('.products-carousel-card-4-item').owlCarousel({
        loop: true,
        margin: 15,
        items: 4,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            767: {
                items: 2
            },
            992: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });

    // also-bought-product-carousel-card
    $('.also-bought-product-carousel-card').owlCarousel({
        loop: true,
        margin: 15,
        items: 4,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            767: {
                items: 2
            },
            992: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });
    
    // top-seller-carousel
    $('.top-seller-carousel').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        centerMode: false,
        focusOnSelect: false,
        arrows: false,
        vertical: true,
        verticalSwiping: true,
        dots: true,
        margin: 0,
    });

    // latest-items-carousel
    $('.latest-items-carousel').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        centerMode: false,
        focusOnSelect: false,
        arrows: false,
        vertical: true,
        verticalSwiping: true,
        dots: true,
    });

    // from-blog-carousel
    $('.from-blog-carousel').slick({
        slidesToShow: 2,
        slidesToScroll: 2,
        centerMode: false,
        focusOnSelect: false,
        arrows: false,
        vertical: true,
        verticalSwiping: true,
        dots: true,
    });

    // product-details carousel
    $(".product-sync-nav").slick({
        dots: false,
        arrows: false,
        infinite: true,
        prevArrow: '<button class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
        nextArrow: '<button class="slick-next"><i class="fas fa-arrow-right"></i></button>',
        slidesToShow: 5,
        slidesToScroll: 1,
        vertical: true,
        verticalSwiping: true,
        asNavFor: ".product-details-image-big",
        focusOnSelect: true,
        draggable: false
    });
    $(".product-details-image-big").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        draggable: false,
        arrows: false,
        dots: false,
        fade: true,
        asNavFor: ".product-sync-nav"
    });
    
    
    
    



    // isotop wrap start
    // isotop
    $('.grid').imagesLoaded( function() {
    	// init Isotope
    	var $grid = $('.grid').isotope({
    	  itemSelector: '.grid-item',
    	  percentPosition: true,
    	  masonry: {
    		// use outer width of grid-sizer for columnWidth
    		columnWidth: '.grid-item',
    	  }
    	});
    });

    // filter items on button click
    $('.portfolio-menu').on( 'click', 'button', function() {
      var filterValue = $(this).attr('data-filter');
      $grid.isotope({ filter: filterValue });
    });

    // for menu active class
    $('.portfolio-menu button').on('click', function(event) {
    	$(this).siblings('.active').removeClass('active');
    	$(this).addClass('active');
    	event.preventDefault();
    });
    // isotop wrap end



    



    // scrollToTop
    $.scrollUp({
    	scrollName: 'scrollUp', // Element ID
    	topDistance: '300', // Distance from top before showing element (px)
    	topSpeed: 300, // Speed back to top (ms)
    	animation: 'fade', // Fade, slide, none
    	animationInSpeed: 200, // Animation in speed (ms)
    	animationOutSpeed: 200, // Animation out speed (ms)
    	scrollText: '<i class="fa fa-sort-up"></i>', // Text for element
    	activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });

})(jQuery);